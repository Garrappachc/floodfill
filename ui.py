# -*- coding: utf-8 -*-

from PyQt4 import QtGui

import config
from board import BoardWidget


class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setup_ui()
        self.w = None
        self.options = {
            'neighbourhood': 0,
            'monte_carlo': False,
            'moore_prob': 90,
            'width': 500,
            'height': 500,
            'delay': 50,
            'bc': False,
            'inclusion_type': 'square',
            'inclusion_size': 1
        }

    def setup_ui(self):
        layout = QtGui.QVBoxLayout()

        nhood_layout = QtGui.QHBoxLayout()
        nhood_layout.addWidget(QtGui.QLabel('Neighbourhood: '))
        nhood_combo = QtGui.QComboBox()
        items = ['Moor', 'Nearest Moore', 'Further Moore', '% Moore', 'von Nauman', 'Hexagonal left', 'Hexagonal right', 'Hexagonal random', 'Pentagonal left',
                 'Pentagonal right', 'Pentagonal top', 'Pentagonal bottom', 'Pentagonal random']
        nhood_combo.addItems(items)
        nhood_combo.currentIndexChanged.connect(self.update_neighbourhood)
        nhood_layout.addWidget(nhood_combo)
        layout.addLayout(nhood_layout)

        layout_prob_moore = QtGui.QHBoxLayout()
        layout_prob_moore.addSpacing(50)
        self.prob_moore_label = QtGui.QLabel('Probability:')
        self.prob_moore_label.hide()
        layout_prob_moore.addWidget(self.prob_moore_label)
        self.prob_moore_spinbox = QtGui.QSpinBox()
        self.prob_moore_spinbox.setMaximum(100)
        self.prob_moore_spinbox.setMinimum(1)
        self.prob_moore_spinbox.setSingleStep(10)
        self.prob_moore_spinbox.setValue(90)
        self.prob_moore_spinbox.hide()
        self.prob_moore_spinbox.valueChanged.connect(self.update_moore_prob)
        layout_prob_moore.addWidget(self.prob_moore_spinbox)
        layout.addLayout(layout_prob_moore)

        mc_checkbox = QtGui.QCheckBox('Monte Carlo')
        mc_checkbox.stateChanged.connect(self.update_montecarlo)
        layout.addWidget(mc_checkbox)

        layout_size = QtGui.QHBoxLayout()
        layout_size.addWidget(QtGui.QLabel('Board size:'))

        width_spinbox = QtGui.QSpinBox()
        width_spinbox.setMaximum(1920)
        width_spinbox.setMinimum(50)
        width_spinbox.setSingleStep(10)
        width_spinbox.setValue(500)
        width_spinbox.valueChanged.connect(self.update_width)
        layout_size.addWidget(width_spinbox)

        layout_size.addWidget(QtGui.QLabel('x'))

        height_spinbox = QtGui.QSpinBox()
        height_spinbox.setMaximum(1080)
        height_spinbox.setMinimum(50)
        height_spinbox.setSingleStep(10)
        height_spinbox.setValue(500)
        height_spinbox.valueChanged.connect(self.update_height)
        layout_size.addWidget(height_spinbox)
        layout.addLayout(layout_size)

        layout_delay = QtGui.QHBoxLayout()
        layout_delay.addWidget(QtGui.QLabel('Delay [ms]:'))

        delay_spinbox = QtGui.QSpinBox()
        delay_spinbox.setMaximum(1000)
        delay_spinbox.setMinimum(1)
        delay_spinbox.setSingleStep(50)
        delay_spinbox.setValue(50)
        delay_spinbox.valueChanged.connect(self.update_delay)
        layout_delay.addWidget(delay_spinbox)
        layout.addLayout(layout_delay)

        layout_inclusions = QtGui.QHBoxLayout()
        layout_inclusions.addWidget(QtGui.QLabel('Inclusion type:'))
        square_radio = QtGui.QRadioButton('square')
        square_radio.setChecked(True)
        square_radio.toggled.connect(self.update_inclusion_type)
        layout_inclusions.addWidget(square_radio)
        round_radio = QtGui.QRadioButton('round')
        layout_inclusions.addWidget(round_radio)
        layout.addLayout(layout_inclusions)

        layout_inclusions2 = QtGui.QHBoxLayout()
        layout_inclusions2.addWidget(QtGui.QLabel('Inclusion size:'))
        inclusion_size = QtGui.QSpinBox()
        inclusion_size.setMaximum(10)
        inclusion_size.setMinimum(1)
        inclusion_size.setSingleStep(1)
        inclusion_size.setValue(1)
        inclusion_size.valueChanged.connect(self.update_inclusion_size)
        layout_inclusions2.addWidget(inclusion_size)
        layout.addLayout(layout_inclusions2)

        bc_checkbox = QtGui.QCheckBox('Boundaries')
        bc_checkbox.stateChanged.connect(self.update_bc)
        layout.addWidget(bc_checkbox)

        start_button = QtGui.QPushButton('Start')
        start_button.clicked.connect(self.start)
        layout.addWidget(start_button)

        self.setLayout(layout)
        self.setWindowTitle('Flood Fill setup')

    def start(self):
        # self.w = BoardWidget(self.options)
        # self.w.resize(self.options['width'], self.options['height'])
        # self.w.setWindowTitle('Flood Fill')
        # self.w.show()
        self.w = QtGui.QWidget()
        board = BoardWidget(self.options)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(board)

        layout2 = QtGui.QHBoxLayout()

        rec_button = QtGui.QPushButton('Recrystallize')
        rec_button.clicked.connect(board.start_recrystallization)
        if self.options['monte_carlo']:
            rec_button.setEnabled(False)
        layout2.addWidget(rec_button)

        dmr_button = QtGui.QPushButton('DMR')
        dmr_button.clicked.connect(board.dmr_toggle)
        if not self.options['monte_carlo']:
            dmr_button.setEnabled(False)
        layout2.addWidget(dmr_button)

        nucleus_menu = QtGui.QMenu()
        nucleus_action_constant = QtGui.QAction('Constant', nucleus_menu)
        nucleus_action_constant.triggered.connect(board.start_nucleus)
        nucleus_action_increasing = QtGui.QAction('Increasing', nucleus_menu)
        nucleus_action_increasing.triggered.connect(lambda: board.start_nucleus(1))
        nucleus_action_decreasing = QtGui.QAction('Decreasing', nucleus_menu)
        nucleus_action_decreasing.triggered.connect(lambda: board.start_nucleus(-1))
        nucleus_menu.addAction(nucleus_action_constant)
        nucleus_menu.addAction(nucleus_action_increasing)
        nucleus_menu.addAction(nucleus_action_decreasing)

        nucleus_button = QtGui.QPushButton('Nucleus')
        nucleus_button.setMenu(nucleus_menu)
        if not self.options['monte_carlo']:
            nucleus_button.setEnabled(False)
        layout2.addWidget(nucleus_button)

        layout.addLayout(layout2)
        self.w.setLayout(layout)

        self.w.resize(self.options['width'], self.options['height'])
        self.w.setWindowTitle('Flood Fill')
        self.w.show()

    def update_neighbourhood(self, index):
        self.options['neighbourhood'] = index
        if index == 3:
            self.prob_moore_label.show()
            self.prob_moore_spinbox.show()
        else:
            self.prob_moore_label.hide()
            self.prob_moore_spinbox.hide()

    def update_moore_prob(self, value):
        self.options['moore_prob'] = value

    def update_montecarlo(self, state):
        self.options['monte_carlo'] = False if state == 0 else True

    def update_width(self, value):
        self.options['width'] = value

    def update_height(self, value):
        self.options['height'] = value

    def update_delay(self, value):
        self.options['delay'] = value

    def update_bc(self, state):
        self.options['bc'] = False if state == 0 else True

    def update_inclusion_type(self, square_state):
        self.options['inclusion_type'] = 'circle' if square_state == 0 else 'square'

    def update_inclusion_size(self, value):
        self.options['inclusion_size'] = value
