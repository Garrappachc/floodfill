#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtGui

import ui


def main():
    app = QtGui.QApplication(sys.argv)

    w = ui.MainWindow()
    w.show()

    return app.exec_()


if __name__ == '__main__':
    r = main()
    sys.exit(r)

