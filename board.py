# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import math

import config
import random
import threading


def dislocation_density(it):
    if it < 0:
        return 0.0
    a = 86710969050178.5
    b = 9.41268203527779
    t = float(it) * float(50) / 1000.0
    r = a / b + (1 - a / b) * math.exp(-b * t)
    return r


def random_dislocation_density(a, b, p):
    return float(random.randint(a, b) / 100.0) * p


def around(x, y):
    for i in range(x - 1, x + 2):
        yield (i, y - 1)
    yield (x + 1, y)
    for i in reversed(range(x - 1, x + 2)):
        yield (i, y + 1)
    yield (x - 1, y)


class Point(QtCore.QObject):
    def __init__(self, p=None):
        super(Point, self).__init__()
        if p is None:
            self.act = 0
            self.prev = 0
            self.rec = 0  # stan zrekrystalizowanej komórki
            self.rec_prev = 0
            self.p = 0  # gęstość dyslokacji
            self.cryst = QtGui.QColor(0, 0, 0)
            self.non_cryst = QtGui.QColor(255, 255, 255)
            self.is_inclusion = False
            self.done = 0
            self.dmr_color = None
            self.is_nucleus = False
        else:
            self.act = p.act
            self.prev = p.prev
            self.rec = p.rec
            self.rec_prev = p.rec_prev
            self.p = p.p
            self.cryst = p.cryst
            self.non_cryst = p.non_cryst
            self.is_inclusion = p.is_inclusion
            self.done = p.done
            self.dmr_color = p.dmr_color
            self.is_nucleus = p.is_nucleus

        if self.is_inclusion:
            self.cryst = QtGui.QColor(0, 0, 0)
            self.non_cryst = QtGui.QColor(0, 0, 0)

    def set_rec(self, x):
        self.rec = x
        self.rec_prev = x
        self.p = 0

        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.cryst = QtGui.QColor(r, g, b)

    def set(self, x):
        self.prev = x
        self.act = x
        self.rec = 0
        self.rec_prev = 0
        self.p = 0
        self.cryst = QtGui.QColor(0, 0, 0)
        self.non_cryst = QtGui.QColor(255, 255, 255)

    def randomize(self, x):
        self.set(x)

        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.non_cryst = QtGui.QColor(r, g, b)

    def recolor(self, p):
        if isinstance(p, Point):
            self.non_cryst = p.non_cryst
        elif isinstance(p, QtGui.QColor):
            self.non_cryst = p

    def recolor_recrystallized(self, p):
        self.cryst = p.cryst

    def color_matches(self, p):
        (r, g, b, a) = self.non_cryst.getRgb()
        if r == 255 and g == 255 and b == 255:
            return False
        if isinstance(p, Point):
            return p.non_cryst == self.non_cryst
        elif isinstance(p, QtGui.QColor):
            return self.non_cryst == p


class BoardWidget(QtGui.QWidget):

    def __init__(self, options, parent=None):
        super(BoardWidget, self).__init__(parent)
        # constants
        self.Act = 1
        self.Rec = 2
        self.DP = 3 # dualphase
        # variables
        self.board_width = options['width'] / config.pixel_size
        self.board_height = options['height'] / config.pixel_size
        self.delay = options['delay']
        self.bc = options['bc']
        self.inclusion_type = options['inclusion_type']
        self.inclusion_size = options['inclusion_size']
        self.moore_prob = options['moore_prob']
        self.started = False
        self.recrystallizing = False
        self.show_dmr = False
        self.nucleus_started = False
        self.nucleus_counter = 10
        self.nucleus_diff = 0
        self.rec_range = 10
        self.p = [[Point() for x in range(self.board_width)] for x in range(self.board_height)]
        self.it = 0
        self.patterns = {
            '0': [1, 1, 1, 1, 1, 1, 1, 1],
            '1': [0, 1, 0, 1, 1, 0, 1, 0],
            '2': [1, 0, 1, 0, 0, 1, 0, 1],
            '3': [1, 1, 1, 1, 1, 1, 1, 1],
            '4': [0, 1, 0, 1, 0, 1, 0, 1],
            '5': [1, 1, 0, 1, 1, 1, 0, 1],
            '6': [0, 1, 1, 1, 0, 1, 1, 1],
            '7': [],
            '8': [1, 1, 0, 0, 0, 1, 1, 1],
            '9': [0, 1, 1, 1, 1, 1, 0, 0],
            '10': [1, 1, 1, 1, 0, 0, 0, 1],
            '11': [0, 0, 0, 1, 1, 1, 1, 1],
            '12': []
        }
        self.options = options
        self.colors = []
        self.initialize_colors()
        self.lock = QtCore.QReadWriteLock()

        self.t = QtCore.QTimer(self)
        self.t.timeout.connect(self.step)
        self.t.start(self.delay)

    def start_recrystallization(self):
        self.recrystallizing = True
        self.started = True

    def dmr_toggle(self):
        if not self.show_dmr:
            self.started = False
            for i in range(self.board_width):
                for j in range(self.board_height):
                    if self.on_border(i, j):
                        self.p[i][j].dmr_color = QtGui.QColor(0, 54, 39)
                    else:
                        self.p[i][j].dmr_color = QtGui.QColor(0, 126, 92)
            self.show_dmr = True
        else:
            self.show_dmr = False
            self.started = True

        self.update()

    def start_nucleus(self, diff=0):
        self.started = True
        self.show_dmr = False
        self.nucleus_started = True
        self.nucleus_diff = diff

    def nucleus(self):
        border_points = []

        for i in range(self.board_width):
            for j in range(self.board_height):
                if not self.p[i][j].is_nucleus and self.on_border(i, j):
                    border_points.append((i, j))

        for i in range(self.nucleus_counter):
            idx = random.randint(0, len(border_points))
            (x, y) = border_points[idx]
            del border_points[idx]
            c = QtGui.QColor(random.randint(100, 255), 0, 0)
            self.p[x][y].recolor(c)
            self.p[x][y].is_nucleus = True

        self.nucleus_counter += self.nucleus_diff
        if self.nucleus_counter <= 1:
            self.nucleus_counter = 1
        self.update()

    def initialize_colors(self):
        for i in range(8):
            c = QtGui.QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            while c in self.colors:
                c = QtGui.QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            self.colors.append(c)

    def step(self):
        if self.started:
            self.save_as_previous()
            self.refresh()

    def iterate_naive(self):
        x = 0

        for i in range(self.board_width):
            for j in range(self.board_height):
                if self.p[i][j].act == 1:
                    continue
                if self.p[i][j].act == self.DP:
                    continue

                x += 1
                tmp = self.find_neighbour(i, j, self.Act)
                if tmp.act < 0 and tmp.prev < 0:
                    continue
                elif tmp.act == self.DP:
                    continue

                self.p[i][j].act = tmp.act
                self.p[i][j].recolor(tmp)
        return x

    def recrystallization(self):
        self.it += 1
        p = dislocation_density(self.it) - dislocation_density(self.it - 1)  # różnica dyslokacji
        psr = p / (self.board_width * self.board_height)  # dyslokacje na jedną komórkę
        p_critical = 4215840142323.42 / (self.board_width * self.board_height)
        p_all = 0.0
        p_real = 0.0
        cryst_count = 0  # ile ziaren zrekrystalizowanych?
        for i in range(self.board_width):
            for j in range(self.board_height):
                if self.p[i][j].rec == 0:
                    if self.on_border(i, j):
                        tmp = random_dislocation_density(70, 170, psr)
                    else:
                        tmp = random_dislocation_density(0, 30, psr)
                    p_all += tmp
                    self.p[i][j].p += tmp
                else:
                    cryst_count += 1
                # ziarno zrekrystalizowane
                if self.p[i][j].rec == 1:
                    continue
                if self.p[i][j].p > p_critical and self.able_to_recrystallize(i, j):  # nowe ziarno
                    self.p[i][j].set_rec(1)

        try:
            p_diff = (p - p_all) / float(self.board_width * self.board_height - cryst_count)
        except ZeroDivisionError:
            return

        for i in self.p:
            for pt in i:
                if pt.rec == 0:
                    pt.p += p_diff
                p_real += pt.p

        for i in range(self.board_width):
            for j in range(self.board_height):
                if self.p[i][j].act == 0 or self.p[i][j].rec == 1:
                    continue
                tmp = self.find_neighbour(i, j, self.Rec)
                if tmp.act < 0:
                    continue
                if tmp.rec == 1 and self.p[i][j].rec == 0:
                    self.p[i][j].p = 0.0
                    self.p[i][j].rec = tmp.rec
                    self.p[i][j].recolor_recrystallized(tmp)

    def monte_carlo(self):
        for i in range(self.board_width):
            for j in range(self.board_height):
                if self.p[i][j].act == self.DP:
                    continue

                colors = self.colors[:]
                e = self.count_energy(i, j)
                while True:
                    n = random.randint(0, len(colors) - 1)
                    col = colors[n]
                    del colors[n]

                    self.p[i][j].recolor(col)
                    new_e = self.count_energy(i, j)
                    if new_e <= e:
                        break
                    elif len(colors) == 0:
                        break

    def refresh(self):
        if self.recrystallizing:
            self.recrystallization()
        elif self.nucleus_started:
            self.nucleus()
        elif self.options['monte_carlo']:
            self.monte_carlo()
        elif config.method == 0:
            self.iterate_naive()

        self.update()

    def neighbourhood_pattern(self):
        if self.options['neighbourhood'] == 7:
            return self.patterns[str(random.randint(0, 1) + 5)]
        elif self.options['neighbourhood'] == 12:
            return self.patterns[str(random.randint(0, 3) + 8)]
        elif self.options['neighbourhood'] == 3:
            return [1 if random.randint(0, 100) < self.moore_prob else 0 for x in range(0, 8)]
        else:
            return self.patterns[str(self.options['neighbourhood'])]

    def find_neighbour(self, x, y, t):
        s = [0, 0, 0, 0, 0, 0, 0, 0]

        idx = 0
        for (i, j) in around(x, y):
            try:
                if t == self.Act:
                    s[idx] = self.p[i][j].prev
                elif t == self.Rec:
                    s[idx] = self.p[i][j].rec_prev
                else:
                    raise ValueError('t not one of [Rec, Act]!')
            except IndexError:
                pass
            idx += 1

        left_x = x - 1
        right_x = x + 1
        top_y = y - 1
        bottom_y = y + 1

        s_pattern = self.neighbourhood_pattern()

        if s[0] == 1 and s_pattern[4] == 1:
            return self.p[left_x][top_y]
        elif s[1] == 1 and s_pattern[5] == 1:
            return self.p[x][top_y]
        elif s[2] == 1 and s_pattern[6] == 1:
            return self.p[right_x][top_y]
        elif s[3] == 1 and s_pattern[7] == 1:
            return self.p[right_x][y]
        elif s[4] == 1 and s_pattern[0] == 1:
            return self.p[right_x][bottom_y]
        elif s[5] == 1 and s_pattern[1] == 1:
            return self.p[x][bottom_y]
        elif s[6] == 1 and s_pattern[2] == 1:
            return self.p[left_x][bottom_y]
        elif s[7] == 1 and s_pattern[3] == 1:
            return self.p[left_x][y]

        p = Point()
        p.set(-1)
        return p

    def closeEvent(self, event):
        self.t.stop()

    def mousePressEvent(self, event):
        px = event.x() / config.pixel_size
        py = event.y() / config.pixel_size

        px = 1 if px < 1 else px
        px = self.board_width - 2 if px > self.board_width - 2 else px
        py = 1 if py < 1 else py
        py = self.board_height - 2 if py > self.board_height - 2 else py

        p = self.p[px][py]

        if event.button() == QtCore.Qt.LeftButton:
            if p.act == 0:
                self.p[px][py] = Point()
                self.p[px][py].randomize(1)
                self.started = True
            elif not p.is_inclusion:
                if event.modifiers() & QtCore.Qt.ControlModifier:
                    self.dualphase(px, py, dp_color=QtGui.QColor(255, 0, 0))
                else:
                    self.dualphase(px, py)
                self.started = False
                self.update()
        elif event.button() == QtCore.Qt.RightButton:
            if p.act == 0:
                d = (int) (self.inclusion_size / 2)
                for dx in range(px - d, px + d):
                    for dy in range(py - d, py + d):
                        point = Point()
                        point.is_inclusion = True
                        self.p[dx][dy] = point
                        self.update()

    def dualphase(self, x, y, **kwargs):
        color = self.p[x][y].non_cryst
        dp_color = color
        if 'dp_color' in kwargs:
            dp_color = kwargs['dp_color']
        for i in self.p:
            for p in i:
                if p.act == self.DP:
                    continue

                if p.color_matches(color):
                    p.act = self.DP
                    p.recolor(dp_color)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        x = 0
        y = 0
        for i in self.p:
            for p in i:
                if p.is_inclusion:
                    if self.inclusion_type == 'square':
                        painter.fillRect(x * config.pixel_size, y * config.pixel_size, config.pixel_size, config.pixel_size,
                                         QtGui.QColor(0, 0, 0))
                elif self.show_dmr:
                    color = p.cryst if p.is_nucleus else p.dmr_color
                    painter.fillRect(x * config.pixel_size, y * config.pixel_size, config.pixel_size, config.pixel_size,
                                     color)
                elif p.cryst != QtGui.QColor(0, 0, 0):
                    painter.fillRect(x * config.pixel_size, y * config.pixel_size, config.pixel_size, config.pixel_size,
                                     p.cryst)
                else:
                    painter.fillRect(x * config.pixel_size, y * config.pixel_size, config.pixel_size, config.pixel_size,
                                     p.non_cryst)
                y += 1
            y = 0
            x += 1

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Space:
            for i in self.p:
                for p in i:
                    if p.act != self.DP:
                        p.act = 0
                        p.recolor(QtGui.QColor(255, 255, 255))
            self.started = True
            self.update()

    def save_as_previous(self):
        for i in self.p:
            for p in i:
                p.prev = p.act
                p.rec_prev = p.rec

    def on_border(self, x, y):
        for (i, j) in around(x, y):
            try:
                if not self.p[i][j].color_matches(self.p[x][y]):
                    return True
            except IndexError:
                return True
        return False

    def able_to_recrystallize(self, x, y):
        for i in range(1, self.rec_range):
            for j in range(1, self.rec_range):
                if y - j >= 0 and x - i >= 0:
                    if self.p[x - i][y - j].rec == 1:
                        return False
                if y + j < self.board_height and x + i < self.board_width:
                    if self.p[x + i][y + j].rec == 1:
                        return False
        return True

    def count_energy(self, x, y):
        s = [0, 0, 0, 0, 0, 0, 0, 0]
        idx = 0
        for (i, j) in around(x, y):
            try:
                s[idx] = self.p[i][j]
            except IndexError:
                if not self.bc:
                    if i < 0:
                        i = self.board_width - 1
                    elif i >= self.board_width:
                        i = 0
                    elif j < 0:
                        j = self.board_height - 1
                    elif j >= self.board_height:
                        j = 0
            idx += 1

        s_pattern = self.neighbourhood_pattern()
        e = 0
        i = 0

        for p in s:
            if not isinstance(p, Point):
                continue

            if s_pattern[i] == 0:
                continue
            i += 1
            if not self.p[x][y].color_matches(p):
                e += 1

        return e

